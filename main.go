package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	ticker := time.NewTicker(10 * time.Second)
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	doneTicker := make(chan bool,1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		for {
			select {
			case <-doneTicker:
				fmt.Println("Ticker stop")
				return
			case <-ticker.C:
				fmt.Println("Doing important work")
			}
		}
	}()

	time.Sleep(10 * time.Second)
	fmt.Println("important work completed")

	go func() {
		<-sigs
		done <- true
		ticker.Stop()
		doneTicker <- true
	}()

	<-done

	doneTicker <- true
	<-doneTicker

	fmt.Println("exiting")
}
